<?php if ( !defined( 'ABSPATH' ) ) exit; ?><!DOCTYPE html>

<html <?php language_attributes(); ?>>

	<head>
		<title><?php echo wp_title( '-', false, 'right' ) ? wp_title( '-', false, 'right' ) : get_bloginfo( 'name' ); ?></title>
		<?php wp_head(); ?>
		<link rel="apple-touch-icon" sizes="57x57" href="/apple-touch-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="/apple-touch-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="/apple-touch-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="/apple-touch-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="/apple-touch-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="/apple-touch-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="/apple-touch-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="/apple-touch-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon-180x180.png">
<link rel="icon" type="image/png" href="/favicon-32x32.png" sizes="32x32">
<link rel="icon" type="image/png" href="/favicon-194x194.png" sizes="194x194">
<link rel="icon" type="image/png" href="/favicon-96x96.png" sizes="96x96">
<link rel="icon" type="image/png" href="/android-chrome-192x192.png" sizes="192x192">
<link rel="icon" type="image/png" href="/favicon-16x16.png" sizes="16x16">
<link rel="manifest" href="/manifest.json">
<meta name="apple-mobile-web-app-title" content="Vitalitist.com">
<meta name="application-name" content="Vitalitist.com">
<meta name="msapplication-TileColor" content="#603cba">
<meta name="msapplication-TileImage" content="/mstile-144x144.png">
<meta name="theme-color" content="#ffffff">
	</head>

	<body <?php body_class(); ?>>

		<div id="layout">

			<div id="header">

				<div id="header-layout-2">

					<div id="header-holder-2">

						<?php

							// Icons the Social
							if ( function_exists( 'st_icons_social' ) ) {
								st_icons_social(); }

							// Menu Secondary
							st_menu_secondary();

						?>

						<form action="<?php echo esc_url( home_url( '/' ) ); ?>" method="get" role="search" id="search-form-header">
							<span></span>
							<input
								type="text"
								name="s"
								value=""
								placeholder="<?php _e( 'Search...', 'strictthemes' ) ?>"
							/>
						</form>

						<div class="clear"><!-- --></div>

					</div><!-- #header-holder-2 -->

				</div><!-- #header-layout-2 -->

				<div id="header-layout">

					<?php
						// Sidebar Ad A
						if ( is_active_sidebar(4) ) {
							get_template_part( '/includes/sidebars/sidebar_ad_a' ); }
					?>

					<div id="header-holder">

						<div id="menu" class="div-as-table <?php global $st_Settings; if ( !empty( $st_Settings['stickymenu'] ) && $st_Settings['stickymenu'] == 'no' ) echo 'no-sticky-menu' ?>">
							<div>
								<div>

									<div id="logo" class="div-as-table">
										<div>
											<div>
												<?php
													// Logo
													st_logo();
												?>
											</div>
										</div>
									</div><!-- #logo -->

									<span id="menu-select"></span>
									<?php
										// Menu Primary
										st_menu_primary();
									?>

									<div class="clear"><!-- --></div>
								</div>
							</div>
						</div><!-- #menu -->

						<div class="clear"><!-- --></div>

					</div><!-- #header-holder -->

				</div><!-- #header-layout -->

			</div><!-- #header -->

			<div id="content-parent">

				<?php
					// Posts sticky
					get_template_part( '/includes/posts/sticky' );
				?>

				<div id="content-layout">

					<?php

						// Posts most viewed
						if ( !empty( $st_Settings['post_views'] ) == 'yes' ) {
							get_template_part( '/includes/posts/most_viewed' ); }

						// Sidebar Ad B
						if ( is_front_page() || is_category() || is_tag() || get_query_var('post_format') ) {
							if ( is_active_sidebar(5) ) {
								get_template_part( '/includes/sidebars/sidebar_ad_b' ); }
						}

					?>
