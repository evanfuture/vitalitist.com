/*

	1 - MENU PRIMARY

		1.1 - Action by hover
		1.2 - Add class: hasUl
		1.3 - Action by click (resp)
		1.4 - 3rd level margin
		1.5 - Responsive menu toggle
		1.6 - Flush menu by click
		1.7 - Flush menu by resize

	2 - MENU SECONDARY

		2.1 - Action by hover
		2.2 - 3rd level margin

	3 - MENU CUSTOM

		3.1 - Span sunbline
		3.2 - Span arrow
		3.3 - Current the class
		3.4 - Add class by hover
		3.5 - Action by click

	4 - THEME RELATED

		4.1 - Related posts
		4.2 - Sticky posts
		4.3 - Projects selector
		4.4 - Author info expand

	5 - COMMON

		5.1 - Detect browser name
		5.2 - Dinamic styles holder
		5.3 - Main menu on responsive mode
		5.4 - Quick reply form
		5.5 - Textarea animation by focus
		5.6 - Tag cloud
		5.7 - Archives widget span
		5.8 - Image original side
		5.9 - Max size for YouTube & Vimeo video
		5.10 - ST Gallery
		5.11 - Sticked menu
		5.12 - Search form on header
		5.13 - BuddyPress fixes
		5.14 - For IE only
		5.15 - WooCommerce
		5.16 - Webkit fixes

	6 - ACTIONS BY LOADING

		6.1 - Flickr widget 150px thumbnails
		6.2 - Sticked div
		6.3 - Scroll to top button

	7 - JAVA SCRIPT

		7.1 - For IE only

*/

/* jshint -W099 */
/* global jQuery:false */

var st = jQuery.noConflict();

st(function(){

	'use strict';

/*==1.===========================================

	M E N U   P R I M A R Y
	Primary menu on header

===============================================*/

	/*-------------------------------------------
		1.1 - Action by hover
	-------------------------------------------*/

	st('.menu li:has(ul), .menu-2 li:has(ul)')

		.hover(

			function(){

				var
					width = st('body').width(),
					li = st(this);

					if ( width > 942 ) {

						li.addClass('hover-has-ul');

					}

			},

			function(){

				var
					width = st('body').width(),
					li = st(this);

					if ( width > 942 ) {

						li.removeClass('hover-has-ul no-transform');

					}

			}

		);


	/*-------------------------------------------
		1.2 - Add class: hasUl
	-------------------------------------------*/

	st('.menu li:has(ul), .menu-2 li:has(ul)').addClass('hasUl');


	/*-------------------------------------------
		1.3 - Action by click (resp)
	-------------------------------------------*/

	st('.menu > li > span.ico-menu-top')

		.click(
			function(){
				/* removed */
			}
		)

		/*-------------------------------------------
			1.4.1 - Add class by hover (resp)
			NOT NECESSARY / Just for resp demo
		-------------------------------------------*/

		.hover(

			function(){

				st(this).parent().addClass('hover-has-ul');

			},

			function(){

				var
					width = st('body').width();

					if ( width < 985 && !st(this).hasClass('clicked') ) {

						st(this).parent().removeClass('hover-has-ul');

					}

			}

		);


	/*-------------------------------------------
		1.4 - 3rd level margin
	-------------------------------------------*/

		st('.menu ul li:has(ul), .menu-2 ul li:has(ul)')
	
			.hover(function(){

				var t = st(this).height() + 5;

				st('ul',this).css({margin: '-' + t + 'px 0 0 0'});

			});


	/*-------------------------------------------
		1.5 - Responsive menu toggle
	-------------------------------------------*/

	/* Super top level */
	st('#menu-select').click(function(){

		if ( st(this).hasClass('resp-menu-opened') ) {

			st_flush_menuByClick();

		}
		else {

			st(this).addClass('resp-menu-opened');
			st('#menu-box').addClass('resp-menu-opened');

			var
				menu = st('#menu-box .menu').html();

				st('#menu-box').append('<ul id="menu-responsive"></ul>');
				st('#menu-responsive').append(menu);
				st('#menu-responsive .sub-menu').removeAttr('style');

				st('#menu-responsive li.hasUl').each(function(){

					st(this).append('<span>&nbsp;</span>');

				});

		}

	});

	/* Sub-levels */
	st('#layout').on('click tap', '#menu-responsive li.hasUl > span', function(){

		var
			submenu = st(this).prev();

			if ( submenu.hasClass('opened') ) {
				submenu.removeClass('opened');
				st(this).removeClass('opened');
			}
			else {
				submenu.addClass('opened');
				st(this).addClass('opened');
			}

	});


	/*-------------------------------------------
		1.7 - Flush menu by click
	-------------------------------------------*/

	function st_flush_menuByClick() {

		st('#menu-responsive').remove();
		st('#menu-box').stop(true, false).removeAttr('style').removeClass('resp-menu-opened');
		st('#menu-select').removeClass('resp-menu-opened');

	}


	/*-------------------------------------------
		1.8 - Flush menu by resize
	-------------------------------------------*/

	st(window).resize(

		function() {

			if ( st('#layout').width() > 942 ) {

				st_flush_menuByClick();
	
			}

		}

	);


/*==2.===========================================

	M E N U   S E C O N D A R Y
	Secondary menu on header

===============================================*/

	// Same as Primary menu


/*==3.===========================================

	M E N U   C U S T O M
	Standard widget

===============================================*/

	/*-------------------------------------------
		3.1 - Span sunbline
	-------------------------------------------*/

	st('.widget_custom_menu > li > a, .widget_custom_menu > li > ul > li > a').each(function(){

		var
			title = st(this).attr('title');

			if ( title ) {
				st(this).append('<span class="subline">' + title + '</span>').removeAttr('title'); }

	});

	/*-------------------------------------------
		3.2 - Span arrow
	-------------------------------------------*/

	st('.widget_custom_menu > li:has(ul)').append('<span>&nbsp;</span>');


	/*-------------------------------------------
		3.3 - Current the class
	-------------------------------------------*/

	st('.widget_custom_menu > li.current-menu-ancestor:has(ul)').addClass('stCurrent');


	/*-------------------------------------------
		3.4 - Add class by hover
	-------------------------------------------*/

	st('.widget_custom_menu span')

		.hover(

			function(){

				st(this).parent().addClass('wHover');

			},

			function(){

				st(this).parent().removeClass('wHover');

			}

		);


	/*-------------------------------------------
		3.5 - Action by click
	-------------------------------------------*/

	st('.widget_custom_menu span').click(function(){

		var
			parent = st(this).parent(),
			menu = st(this).prev();

			if ( st(parent).hasClass('stCurrent') ) {

				st(menu)
					.stop(true, false)
					.animate({ 'height': 0 }, 400,

						function(){
							st(menu).removeAttr('style');
							st(parent).removeClass('stCurrent');
						}

					);

			}

			else {

				st(parent).addClass('stCurrent');

				st(menu).css({ 'display': 'block' });

				var
					height = st(this).prev().outerHeight(true);

					st(menu)
						.css({ 'height': 0 })
						.stop(true, false)
						.animate({ 'height': height }, 400,
	
							function(){
								st(menu).removeAttr('style');
							}

						);

			}

	});



/*==4.===========================================

	T H E M E   R E L A T E D
	Scritps for certain theme

===============================================*/

	/*-------------------------------------------
		4.1 - Related posts
	-------------------------------------------*/

	function st_related_posts_height() {

		var
			rHeightDetails = 0;

			st('.posts-related-details-wrapper > div').each(function(){

				if ( st(this).outerHeight() > rHeightDetails ) {
					rHeightDetails = st(this).outerHeight(); }

			});

			st('.posts-related-wrapper .posts-related-details-wrapper').stop(true, false).animate({ 'height': rHeightDetails }, 250, function(){ st(this).eq(0).animate({ 'opacity': '1' }, 500); });

	}

	function st_the_related_posts_height() {
		setTimeout( st_related_posts_height, 1000 );
	}

	st_the_related_posts_height();

	st(window).resize( st_the_related_posts_height );


	/*-------------------------------------------
		4.2 - Sticky posts
	-------------------------------------------*/

	if ( st('#posts-sticky').length ) {

		var
			bg = st('#posts-sticky .post-sticky-recent a').attr('data-bg');

			if ( bg ) {
				st('#st-dynamic-css').append("#posts-sticky:before { background-image: url('" + bg + "'); }"); }

	}


	/*-------------------------------------------
		4.3 - Projects selector
	-------------------------------------------*/

	st('#projects-selector a').on('click touchstart', function(){

		var
			category = st(this).attr('data-slug');

			// Deselect current link
			st(this).parents('#projects-selector').find('.selected').removeClass('selected');
	
			// Select the clicked link
			st(this).addClass('selected');

			// Project
			st('#projects-selection > div.project-t5').each(function(){

				var
					pr = st(this),
					rotated = 'project-t5-rotated',
					inactive = 'project-t5-inactive',
					inactiveOut = 'project-t5-inactive-out',
					check = pr.attr( 'data-category' ).match( category );

					// If back side (inactive)
					if ( pr.hasClass( rotated ) ) {

						if ( check !== null ) {
							pr.addClass( inactiveOut ).removeClass( rotated ); }

					}
					// If picture (active)
					else {

						if ( check === null ) {
							pr.addClass( inactive ).addClass( rotated ); }

					}
					// Flush all
					if ( category === 'all' ) {
						pr.addClass( inactiveOut ).removeClass( rotated ); }

					setTimeout(
						function(){
							if ( pr.hasClass( 'project-t5-inactive-out' ) ) {
								pr.removeClass( 'project-t5-inactive project-t5-inactive-out' ); }
						},
						500
					);

			});

		return false;

	});


	/*-------------------------------------------
		4.4 - Author info expand
	-------------------------------------------*/

	st('.author .status-content.cutted > div, .page-template-template-authors-php .status-content.cutted > div').on('click tap', function(){

		st(this).parent().removeClass('cutted');

	});



/*==5.===========================================

 	C O M M O N
	Common scripts

===============================================*/

	/*-------------------------------------------
		5.1 - Detect browser name
	-------------------------------------------*/

	function st_browser(){

		var label = navigator.userAgent.match(/(opera|chrome|safari|firefox(?=\/))\/?\s*(\d+)/i) || [];

		if ( label[1] ) {

			st('body')

				.removeClass('opera chrome safari gecko') // Flush classes due page cache

				.addClass( label[1] === 'Firefox' ? 'gecko' : label[1].toLowerCase() );

		}

	}

	st_browser();


	/*-------------------------------------------
		5.2 - Holder for dinamic styles
	-------------------------------------------*/

	if ( !st('#st-dynamic-css').length ) {

		st('head').append('<style id="st-dynamic-css" type="text/css"></style>');

	}


	/*-------------------------------------------
		5.3 - Main menu on responsive mode
	-------------------------------------------*/

	st('#selectElement').change(function() {

		if ( st(this).val() ) {
			window.open( st(this).val(), '_parent' ); }

	});


	/*-------------------------------------------
		5.4 - Quick reply form
	-------------------------------------------*/

		/*- 5.4.1 - Open form
		-------------------------------------------*/
	
		st('a.quick-reply').click(function(){
	
	
			/*--- First of all -----------------------------*/
	
			// Make previous Reply link visible
			st('.quick-reply').removeClass('none');
	
			// Make previous Cancel Reply link hidden
			st('.quick-reply-cancel').addClass('none');
	
			// Erase all quick holders
			st('.quick-holder').html('');
	
			// Make comment form visible
			st('#commentform').removeClass('none');
	
	
			/*--- Append new form -----------------------------*/
	
			var
				id = st(this).attr('title'),
				form = st('#respond').html();
	
				// Make this Reply link hidden
				st(this).addClass('none');
	
				// Make this Cancel Reply link visible
				st(this).next().removeClass('none');
	
				// Hide major form
				st('#commentform, #reply-title').addClass('none');
	
				// Put the form to the holder
				st('#quick-holder-' + id).append(form).find('h3').remove();
	
				// Set an ID for hidden field
				st('#quick-holder-' + id + ' input[name="comment_parent"]').val(id);
	
				// Fix placeholders for IE8,9
				if ( st('body').hasClass('ie8') || st('body').hasClass('ie9') ) {
					
					st('.input-text-box input[type="text"], .input-text-box input[type="email"], .input-text-box input[type="url"]', '#quick-holder-' + id).each( function(){ st(this).val( st(this).attr('placeholder') ); } );
	
				}
	
	
			return false;
	
		});


		/*- 5.4.2 - Cancel reply
		-------------------------------------------*/
	
		st('.quick-reply-cancel').click(function(){
	
			// Make previous Reply link visible
			st('.quick-reply').removeClass('none');
	
			// Make this Cancel Reply link hidden
			st(this).addClass('none');
	
			// Erase all quick holders
			st('.quick-holder').html('');
	
			// Make comment form visible
			st('#commentform, #reply-title').removeClass('none');
	
			return false;
	
		});


	/*-------------------------------------------
		5.5 - Textarea animation by focus
	-------------------------------------------*/

	st('#layout').on('focus', 'textarea', function() {

		if ( !st(this).is('#whats-new') && st(this).height() < 151 && ! st(this).hasClass( 'height-ready' ) ) {

			st(this)
				.css({ height: 70 })
				.animate({ height: 150 }, 300, function(){ st(this).addClass( 'height-ready' ); });

		}

	});


	/*-------------------------------------------
		5.6 - Tag cloud
	-------------------------------------------*/

	st('.tagcloud a').each(function(){

		var
			number = st(this).attr('title').split(' ');

			number = '<span>' + number[0] + '</span>';

			st(this).append(number).attr('title','');

	});


	/*-------------------------------------------
		5.7 - Archives widget span
	-------------------------------------------*/

	st('.widget_archive li, .widget_categories li, .product-categories li').each(function(){

		var
			str = st(this).html();

			str = str.replace(/\(/g,"<span>");
			str = str.replace(/\)/g,"</span>");
			
			st(this).html(str);

	});


	/*-------------------------------------------
		5.8 - Image original side
	-------------------------------------------*/

	st('.size-original').removeAttr('width').removeAttr('height');


	/*-------------------------------------------
		5.9 - Max size for YouTube & Vimeo video
	-------------------------------------------*/

	function st_video_resize(){

		st('iframe').each(function(){

			var
				src = st(this).attr('src');

				if ( src ) {

					var
						check_youtube = src.split('youtube.com'),
						check_vimeo = src.split('vimeo.com'),
						check_ted = src.split('ted.com'),
						check_ustream = src.split('ustream.tv'),
						check_metacafe = src.split('metacafe.com'),
						check_rutube = src.split('rutube.ru'),
						check_mailru = src.split('video.mail.ru'),
						check_vk = src.split('vk.com'),
						check_yandex = src.split('video.yandex'),
						check_dailymotion = src.split('dailymotion.com');
		
						if (
							check_youtube[1] ||
							check_vimeo[1] ||
							check_ted[1] ||
							check_ustream[1] ||
							check_metacafe[1] ||
							check_rutube[1] ||
							check_mailru[1] ||
							check_vk[1] ||
							check_yandex[1] ||
							check_dailymotion[1]
							) {
		
								var
									parentWidth = st(this).parent().width(),
									w = st(this).attr('width') ? st(this).attr('width') : 0,
									h = st(this).attr('height') ? st(this).attr('height') : 0,
									ratio = h / w,
									height = parentWidth * ratio;
			
									if ( w > 1 ) {
										st(this).css({ 'width': parentWidth, 'height': height }); }
		
						}

				}

		});

	}

	st_video_resize();

	st(window).resize( st_video_resize );

	// BuddyPress
	if ( st('#buddypress').length ) {
		setInterval( st_video_resize, 3000 ); }


	/*-------------------------------------------
		5.10 - ST Gallery script
	-------------------------------------------*/

	stG_init();
	
	function stG_init() {

		st('.st-gallery').each(function(){

			st('img',this).addClass('st-gallery-pending').last().addClass('st-gallery-last');

			var
				slides = st(this).html(),
				check = slides.split('img'),
				controls = '<ol>';

				for ( var i = 1; i < check.length; i++ ) {
					if ( i === 1 ) {
						controls += '<li class="st-gallery-tab-current"></li>'; }
					else {
						controls += '<li></li>'; }
				}

				controls += '</ol>';

				st(this).html( '<div>' + slides + '</div>' + controls );

				st('div img:first-child',this).removeClass('st-gallery-pending').addClass('st-gallery-current');

		});

	}

	st('.st-gallery div img').on( 'click tap', function(){

		if ( ! st(this).parent().hasClass('st-gallery-locked') ) {

			var
				img = st(this),
				gallery = st(this).parent(),
				current = gallery.find('.st-gallery-current'),
				hCurrent = gallery.height(),
				imgIndex = img.prevAll().length,
				tabs = img.parent().next( 'ol' );

				gallery.addClass('st-gallery-locked');

				var
					nextImage = ( current.hasClass('st-gallery-last') ? gallery.find('img').first() : gallery.children().eq( imgIndex + 1 ) );

					current
						.removeClass('st-gallery-current').addClass('st-gallery-flushed').stop(true,false)
						.animate({ 'opacity': 0 }, 300,
							function(){
								st(this).removeAttr('style').removeClass('st-gallery-flushed').addClass('st-gallery-pending');
								gallery.removeClass('st-gallery-locked');
							});

					nextImage.removeClass('st-gallery-pending').addClass('st-gallery-current');

					var
						hNext = nextImage.height();

						if ( hNext !== 0 ) {
							gallery.css( 'height', hCurrent ).stop(true,false).animate({ 'height': hNext }, 700 ); }
						else {
							gallery.css( 'height', 'auto' ); }

					var
						currentTab = nextImage.prevAll().length;
	
						tabs.children( '.st-gallery-tab-current' ).removeClass( 'st-gallery-tab-current' );
						tabs.children().eq( currentTab ).addClass( 'st-gallery-tab-current' );

		}

	});

	st('.st-gallery ol li').click(function(){

		st(this).each(function(){

			var
				no = st(this).prevAll().length,
				gallery = st(this).parent().parent().find('div'),
				current = gallery.find('.st-gallery-current'),
				h = gallery.children().eq( no ).height();

				st(this).parent().find('.st-gallery-tab-current').removeClass('st-gallery-tab-current');
				st(this).addClass('st-gallery-tab-current');

				current.removeClass('st-gallery-current').addClass('st-gallery-pending');

				gallery.css( 'height', h );

				gallery.children().eq( no )
					.removeClass('st-gallery-pending')
					.addClass('st-gallery-flushed')
					.css({ opacity: 0 })
					.removeClass('st-gallery-flushed')
					.addClass('st-gallery-current')
					.removeAttr('style');

				gallery.removeAttr('style');

		});

	});


	/*-------------------------------------------
		5.11 - Sticked primary menu
	-------------------------------------------*/

	function st_sticky_menu_reset() {

		st('#menu').removeClass('menu-sticky menu-sticky-now').removeAttr('style');
		st('#header-holder').removeAttr('style');

	}

	function st_sticky_menu() {

		if ( !st('body').hasClass('ie8') && st('#menu').length && !st('#menu').hasClass('no-sticky-menu') ) {

			var
				el = st('#menu'),
				stickyTop = st('#menu').offset().top,
				stickyHeight = st('#header-holder').outerHeight(true),
				margin = st('#wpadminbar').length ? st('#wpadminbar').height() : 0;

				// Flushing
				if ( el.hasClass('menu-sticky') ) {

					el.css({ opacity: 1, top: 'auto' }).removeClass('menu-sticky').removeClass('menu-sticky-now');
					st('#header-holder').css({ paddingBottom: 0, height: 'auto' });

				}

				st(window).scroll(function(){

					if ( st('#content-holder').width() > 934 ) {

						var
							windowTop = st(window).scrollTop();
		
		
							if ( stickyTop < windowTop - 100 ) {
	
								if ( !el.hasClass('menu-sticky') ) {
									el.addClass('menu-sticky').addClass('menu-sticky-now').css({ opacity: 1, top: -stickyHeight }).stop(true,false).animate({ top: 0 + margin }, 300);
									st('#header-holder').css({ height: stickyHeight });
								}
		
							}
		
							else {
	
								if ( el.hasClass('menu-sticky-now') ) {

									el.removeClass('menu-sticky-now').stop(true,false).animate({ top: -stickyHeight }, 300,
										function(){
											el.css({ 'display': 'table', top: 0, opacity: 0 }).removeClass('menu-sticky').stop(true,false).animate({ top: 0, opacity: 1 }, 300, function(){ st('#menu').removeAttr('style'); });
											st('#header-holder').css({ height: 'auto' });
										});

								}
	
							}

					}

					else {

						if ( el.hasClass('menu-sticky') ) {

							el.css({ opacity: 1, top: 'auto' }).removeClass('menu-sticky');
							st('#header-holder').css({ height: 'auto' });

						}

					}

				});

		}

	}

	setTimeout( st_sticky_menu, 1000 );

	st(window).resize( st_sticky_menu_reset );


	/*-------------------------------------------
		5.12 - Search form on header
	-------------------------------------------*/

	st('#layout').on('mousedown touchstart', '#search-form-header span', function(){

		if ( !st(this).parent().hasClass('search-form-header') ) {
			st(this).parent().addClass('search-form-header'); }

		else {
			st(this).parent().removeClass('search-form-header');	}

	});

	st('#search-form-header').on( 'mouseenter', 'span', function() {
		st('#search-form-header span').parent().addClass('search-form-header');
	});


	/*-------------------------------------------
		5.13 - BuddyPress fixes
	-------------------------------------------*/

	st('body.group-create h1.page-title a').addClass('button').addClass('bp-title-button');


	/*-------------------------------------------
		5.14 - IE fixes
	-------------------------------------------*/

	/*
	
		5.14.1 - Quick reply form
		5.14.2 - OnBlur/OnFocus for input fields
		5.14.3 - Dummy Search
		5.14.4 - Dummy Subscribe
	
	*/

	if ( st('#ie9-detect').length ) { st('body').addClass('ie9'); }

	if ( st('body').hasClass('ie8') || st('body').hasClass('ie9') ) {
	

		/*- 5.14.1 - Append and remove quick form
		===========================================*/
	
		/*
		
			5.14.1 - QUICK REPLY FORM
		
				5.14.1.1 - Remove dummy before submiting
				5.14.1.2 - Return dummy after unsuccess submiting
		
		*/
	
			/*- 5.14.1.1 - Remove a dummy before submitting
			-------------------------------------------*/
		
			st('#layout')
		
				.on('mousedown tap', '.form-submit input[type="submit"]', function(){
		
					st(this).parent().parent().find('input[type="text"]')
						.each(function(){
		
							var
								dummy = st(this).attr('placeholder'),
								val = st(this).val();
				
								if ( dummy === val ) {
									st(this).val(''); }
		
						});
		
				});
		
		
			/*- 5.14.1.2 - Return a dummy after unsuccess submitting
			-------------------------------------------*/
		
			st('body').on('ready mouseenter tap', '#layout', function(){
		
				st('input[type="text"]',this).each(function(){
		
					var
						dummy = st(this).attr('placeholder'),
						val = st(this).val();
		
						if ( !val ) {
							st(this).val(dummy); }
		
				});
		
			});
	
	
		/*- 5.14.2 - For input fields
		===========================================*/
	
		st('#layout')
	
			.on('focus', 'input[type="text"]', function(){
	
				var
					dummy = st(this).attr('placeholder'),
					val = st(this).val();
	
					if ( dummy === val ) {
						st(this).val(''); }
	
				})
	
			.on('blur', 'input[type="text"]', function(){
	
				var
					dummy = st(this).attr('placeholder'),
					val = st(this).val();
	
					if ( !val ) {
						st(this).val(dummy); }
	
				});
	
	
		/*- 5.14.3 - Dummy data for search input field
		===========================================*/
	
		st('.searchform').each(function(){
	
			var
				dummy = st('input[type="submit"]',this).val();
	
				st('input[name="s"]',this).val(dummy).attr('placeholder', dummy);
	
		});
	
	
		/*- 5.14.4 - Dummy data for subscribe form
		===========================================*/
	
		st('.feedemail-input').each(function(){
	
			var
				dummy = st(this).attr('placeholder');
	
				st(this).val(dummy);
	
		});


	} // if ( st('body').hasClass('ie8') || st('body').hasClass('ie9') )


	/*-------------------------------------------
		5.15 - WooCommerce
	-------------------------------------------*/

	/*
	
		5.15.1 - Re-build thumbnail
		5.15.2 - Add class to Read More button
		5.15.3 - Remove the First class
		5.15.4 - Replace upsell products
		5.15.5 - Replace related products
		5.15.6 - Replace crosssell products
	
	*/

	if ( st('div').hasClass('woocommerce') || st('body').hasClass('woocommerce-page') ) {
	
	
		/*- 5.15.1 - Re-build thumbnail
		-------------------------------------------*/
	
		st('ul.products > li > a:first-child').each(function(){
	
			var
				sale = st('.onsale',this).length ? st('.onsale',this)[0].outerHTML : '',
				img = st('img',this).length ? st('img',this)[0].outerHTML : '',
				title = st('h3',this).length ? st('h3',this)[0].outerHTML : '',
				rating = st('.star-rating',this).length ? st('.star-rating',this)[0].outerHTML : '',
				price = st('.price',this).length ? st('.price',this)[0].outerHTML : '';
	
				// Replace everything
				st(this).html( '<div class="div-as-table st-woo-hover"><div><div>' + title + rating + '</div></div></div>' + sale + img );
	
				// Replace a price
				st(this).parent().append( '<div class="div-as-table st-woo-price"><div><div>' + price + '</div></div></div>' );
	
				// Add class when it ready be shown
				st(this).parent().addClass('st-woo');
	
		});


		/*- 5.15.2 - Add class to Read More button
		-------------------------------------------*/
	
		st('a.add_to_cart_button').each(function(){
	
			var
				check = st(this).attr('href');
	
				check = check.split('?add-to-cart=');
	
				if ( check[1] === undefined ) {
					st(this).addClass('read_more_button'); }
	
		});


		/*- 5.15.3 - Remove the First class
		-------------------------------------------*/
	
		st('.woocommerce .product .thumbnails a').each(function(){
	
			st(this).removeClass('first').removeClass('last');
	
		});


		/*- 5.15.4 - Replace upsell products
		-------------------------------------------*/
	
		st('.woocommerce .product .upsells').each(function(){
	
			st('li.last',this).removeClass('last');
			st('li.first',this).removeClass('first');
	
			var
				related = st(this).html();
	
				st(this).parent().parent().append( '<div class="st-woo-upsells">' + related + '<div class="clear"></div></div>' );
	
				st(this).remove();
	
		});


		/*- 5.15.5 - Replace related products
		-------------------------------------------*/
	
		st('.woocommerce .product .related').each(function(){
	
			st('li.last',this).removeClass('last');
			st('li.first',this).removeClass('first');
	
			var
				related = st(this).html();
	
				st(this).parent().parent().append( '<div class="st-woo-related">' + related + '<div class="clear"></div></div>' );
	
				st(this).remove();
	
		});


		/*- 5.15.6 - Replace crosssell products
		-------------------------------------------*/
	
		st('.woocommerce .cross-sells').each(function(){
	
			var
				cross = st(this).html();
	
				st(this).html( '<div class="st-woo-cross">' + cross + '<div class="clear"></div></div>' );
	
				//st(this).remove();
	
		});
	
	
	} // if ( st('body').hasClass('woocommerce') )


	/*-------------------------------------------
		5.16 - Webkit fixes
	-------------------------------------------*/

	st('body.chrome select, body.safari select').each(function(){

		var
			tag = st(this).parent().prop('tagName');

			if ( tag !== 'LABEL' ) {

				st(this).wrap('<label class="st-select-label"></label>');

			}

	});


});



/*==6.===========================================

	A C T I O N S   B Y   L O A D I N G
	Starts after complete loading

===============================================*/

var pl = jQuery.noConflict();

jQuery(document).ready(function() {


	/*-------------------------------------------
		6.1 - Flickr widget 150px thumbnails
	-------------------------------------------*/

	pl('.flickr_badge_image img')

		.attr({

			src: function() {

				return pl(this).attr('src').replace('_s.jpg','_q.jpg');

			}
			
		})

		.removeAttr('width height');


	/*-------------------------------------------
		6.2 - Sticked div
	-------------------------------------------*/

	function st_sticky_div() {

		if ( pl('#stickyDiv').length ) {

			var
				stickyDiv = pl('#stickyDiv'),
				stickyTop = pl('#stickyDiv').offset().top,
				margin = pl('#wpadminbar').length ? pl('#wpadminbar').height() : 0;

				pl(window).scroll(function(){

					var
						stickyHeight = pl('#stickyDiv').outerHeight(true),
						limit = pl('#articleEnd').offset().top - stickyHeight,
						windowTop = pl(window).scrollTop(),
						diff = pl('body').hasClass('gecko') ? 100 : 70;


						/*--- by top -----------------------------*/
	
						if ( stickyTop < windowTop + diff + margin ) {

							stickyDiv.css({ position: 'fixed', top: diff + margin });

						}
	
						else {

							stickyDiv.css( 'position', 'static' );
	
						}

	
						/*--- by down -----------------------------*/
	
						if ( limit < windowTop + diff ) {
							
							var
								di = limit - windowTop;

								stickyDiv.css({ position: 'fixed', top: di });

						}

				});

		}

	}

	setTimeout( st_sticky_div, 1000 );


	/*-------------------------------------------
		6.3 - Scroll to top button
	-------------------------------------------*/

	pl('body').append('<div id="scroll-to-top" style="display: none;"><!-- Scroll to top --></div>');

	pl('#scroll-to-top').on('click', function(){ pl('html, body').animate({ scrollTop: 0 }, 1000 ); return false; });

	pl(window).scroll( function(){

		if ( pl(this).scrollTop() > 1000 ) {
			pl('#scroll-to-top').fadeIn(); }

		else {
			pl('#scroll-to-top').fadeOut(); }

	});


});



/*==7.===========================================

	J A V A   S C R I P T
	Pure JS

===============================================*/

	/*-------------------------------------------
		7.1 - For IE only
	-------------------------------------------*/

	var
		ie8check = document.getElementById('ie8-detect');

		if ( ie8check !== null && ie8check.value === undefined ) {
			st('body').addClass('ie8');	}


