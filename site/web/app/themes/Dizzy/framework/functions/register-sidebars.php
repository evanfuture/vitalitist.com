<?php if ( !defined( 'ABSPATH' ) ) exit;

/*

	1 - RETRIEVE DATA

	2 - SIDEBARS

		- Default Sidebar
		- Secondary Sidebar
		- Post Sidebar
		- Ad Sidebars
		- Homepage Sidebars
		- Projects Sidebar
		- Footer Sidebars
		- bbPress Sidebar
		- Additional Sidebars

*/

	function st_register_sidebars() {


		/*===============================================
		
			R E T R I E V E   D A T A
			Get a required data
		
		===============================================*/
		
			global
				$st_Options,
				$st_Settings;
		
				$st_ = array();
		
				// Get a qty of additional sidebars
				$st_['qty'] = !empty( $st_Settings['sidebar_qty'] ) ? $st_Settings['sidebar_qty'] : 0;
		
		
		/*===============================================
		
			S I D E B A R S
			Register a new sidebars
		
		===============================================*/
		
			// Default Sidebar
			if ( $st_Options['sidebars']['default'] ) {
				register_sidebars(
					1,
					array(
						'name'				=> 'Default Sidebar',
						'description'		=> __( 'Appears on posts and pages by default.', 'strictthemes' ),
						'before_widget'		=> "\n" . '<div class="widget %2$s">' . "\n",
						'after_widget'		=> "\n" . '<div class="clear"><!-- --></div></div>' . "\n",
						'before_title'		=> '<h5><span> &nbsp;',
						'after_title'		=> '&nbsp; </span></h5>' . "\n"
					)
				);
			}
		
		
			// Secondary Sidebar
			if ( $st_Options['sidebars']['secondary'] ) {
				register_sidebars(
					1,
					array(
						'name'				=> 'Secondary Sidebar',
						'description'		=> __( 'Appears on archives and homepage.', 'strictthemes' ),
						'before_widget'		=> "\n" . '<div class="widget %2$s">' . "\n",
						'after_widget'		=> "\n" . '<div class="clear"><!-- --></div></div>' . "\n",
						'before_title'		=> '<h5><span> &nbsp;',
						'after_title'		=> '&nbsp; </span></h5>' . "\n"
					)
				);
			}
		
		
			// Post Sidebar
			if ( $st_Options['sidebars']['post-sidebar'] ) {
				register_sidebars(
					1,
					array(
						'name'				=> 'Post Sidebar',
						'description'		=> __( 'Additional sidebar. Usually, it comes by left side of post. Appears on posts only.', 'strictthemes' ),
						'before_widget'		=> "\n" . '<div class="widget %2$s">' . "\n",
						'after_widget'		=> "\n" . '<div class="clear"><!-- --></div></div>' . "\n",
						'before_title'		=> '<h5><span> &nbsp;',
						'after_title'		=> '&nbsp; </span></h5>' . "\n"
					)
				);
			}
		
		
			// Ad Sidebars
			if ( $st_Options['sidebars']['ads'] ) {
		
				$st_['count'] = 0;
		
				foreach ($st_Options['sidebars']['ads'] as $key ) {
		
					$st_['count']++;
		
					register_sidebars(
						1,
						array(
							'name'				=> 'Ad Sidebar ' . $st_['count'],
							'description'		=> $key,
							'before_widget'		=> "\n" . '<div class="widget %2$s">' . "\n",
							'after_widget'		=> "\n" . '<div class="clear"><!-- --></div></div>' . "\n",
							'before_title'		=> '<h5><span> &nbsp;',
							'after_title'		=> '&nbsp; </span></h5>' . "\n"
						)
					);
		
				}
		
			}
		
		
			// Homepage Sidebars
			if ( $st_Options['sidebars']['homepage'] ) {
		
				$st_['count'] = 0;
		
				foreach ( $st_Options['sidebars']['homepage'] as $key ) {
		
					$st_['count']++;
		
					register_sidebars(
						1,
						array(
							'name'				=> 'Homepage Sidebar ' . $st_['count'],
							'description'		=> $key,
							'before_widget'		=> "\n" . '<div class="widget %2$s">' . "\n",
							'after_widget'		=> "\n" . '<div class="clear"><!-- --></div></div>' . "\n",
							'before_title'		=> '<h5><span> &nbsp;',
							'after_title'		=> '&nbsp; </span></h5>' . "\n"
						)
					);
		
				}
		
			}
		
		
			// Projects Sidebar
			if ( $st_Options['sidebars']['projects'] ) {
				register_sidebars(
					1,
					array(
						'name'				=> 'Projects Sidebar',
						'description'		=> __( 'Appears on projects and archives of projects depends of selected template.', 'strictthemes' ),
						'before_widget'		=> "\n" . '<div class="widget %2$s">' . "\n",
						'after_widget'		=> "\n" . '<div class="clear"><!-- --></div></div>' . "\n",
						'before_title'		=> '<h5><span> &nbsp;',
						'after_title'		=> '&nbsp; </span></h5>' . "\n"
					)
				);
			}
		
		
			// Footer Sidebars
			if ( $st_Options['sidebars']['footer'] ) {
				register_sidebars(
					$st_Options['sidebars']['footer'],
					array(
						'name'				=> 'Footer Sidebar %d',
						'description'		=> __( 'Appears on all pages at the bottom of site.', 'strictthemes' ),
						'before_widget'		=> "\n" . '<div class="widget %2$s">' . "\n",
						'after_widget'		=> "\n" . '<div class="clear"><!-- --></div></div>' . "\n",
						'before_title'		=> '<h5><span> &nbsp;',
						'after_title'		=> '&nbsp; </span></h5>' . "\n"
					)
				);
			}
		
		
			// bbPress Sidebar
			if ( $st_Options['sidebars']['bbPress'] ) {
				register_sidebars(
					1,
					array(
						'name'				=> 'bbPress Sidebar',
						'description'		=> __( 'Appears on bbPress forum pages.', 'strictthemes' ),
						'before_widget'		=> "\n" . '<div class="widget %2$s">' . "\n",
						'after_widget'		=> "\n" . '<div class="clear"><!-- --></div></div>' . "\n",
						'before_title'		=> '<h5><span> &nbsp;',
						'after_title'		=> '&nbsp; </span></h5>' . "\n"
					)
				);
			}
		
		
			// BuddyPress Sidebar
			if ( $st_Options['sidebars']['buddyPress'] ) {
				register_sidebars(
					1,
					array(
						'name'				=> 'BuddyPress Sidebar',
						'description'		=> __( 'Appears on BuddyPress pages.', 'strictthemes' ),
						'before_widget'		=> "\n" . '<div class="widget %2$s">' . "\n",
						'after_widget'		=> "\n" . '<div class="clear"><!-- --></div></div>' . "\n",
						'before_title'		=> '<h5><span> &nbsp;',
						'after_title'		=> '&nbsp; </span></h5>' . "\n"
					)
				);
			}
		
		
			// WooCommerce Sidebar
			if ( $st_Options['sidebars']['woocommerce'] ) {
				register_sidebars(
					1,
					array(
						'name'				=> 'WooCommerce Sidebar',
						'description'		=> __( 'Appears on WooCommerce pages.', 'strictthemes' ),
						'before_widget'		=> "\n" . '<div class="widget %2$s">' . "\n",
						'after_widget'		=> "\n" . '<div class="clear"><!-- --></div></div>' . "\n",
						'before_title'		=> '<h5><span> &nbsp;',
						'after_title'		=> '&nbsp; </span></h5>' . "\n"
					)
				);
			}
		
		
			// Additional Sidebars
			register_sidebars(
				$st_['qty'],
				array(
					'name'				=> 'Custom bar %d',
					'description'		=> __( 'Appears on selected posts and pages.', 'strictthemes' ),
					'before_widget'		=> "\n" . '<div class="widget %2$s">' . "\n",
					'after_widget'		=> "\n" . '<div class="clear"><!-- --></div></div>' . "\n",
					'before_title'		=> '<h5><span> &nbsp;',
					'after_title'		=> '&nbsp; </span></h5>' . "\n"
				)
			);


	}
	
	add_action( 'widgets_init', 'st_register_sidebars' );

?>