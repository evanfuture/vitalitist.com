<?php if ( !defined( 'ABSPATH' ) ) exit;

/*

	Template Name: Authors

	1 - RETRIEVE DATA

	2 - PAGE

		2.1 - Default content
		2.2 - Authors
		2.3 - Pagination

*/

/*===============================================

	R E T R I E V E   D A T A
	Get a required page data

===============================================*/

	global
		$st_Settings;

		$st_ = array();

		// Is title disabled?
		$st_['title_disabled'] = st_get_post_meta( $post->ID, 'disable_title_value', true, 0 );

		// Is breadcrumbs disabled?
		$st_['breadcrumbs_disabled'] = st_get_post_meta( $post->ID, 'disable_breadcrumbs_value', true, 0 );

		// Subtitle
		$st_['subtitle'] = get_post_meta( $post->ID, 'subtitle_value', true );

		// Get custom sidebar
		$st_['sidebar'] = st_get_post_meta( $post->ID, 'sidebar_value', true, 'Default Sidebar' );

		// Get sidebar position
		$st_['sidebar_position'] = 'none';

			// Re-define global $content_width if sidebar not exists
			if ( $st_['sidebar_position'] == 'none' ) {
				$content_width = $st_Options['global']['images']['large']['width']; }
			else {
				$content_width = $st_Options['global']['images']['archive-image']['width']; }

		// Is authors
		$st_['is_authors'] = true;

		// Counter
		$st_['count'] = 0;

		// Qty per page
		$st_['per_page'] = 20;

		// Paged
		$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;

		// Get authors
		$st_['authors'] = get_transient( 'st_authors_by_post_count' );

		if ( empty( $st_['authors'] ) ) {

			$st_['authors'] = st_get_authors_by_post_count();

			set_transient( 'st_authors_by_post_count', $st_['authors'], 60 * 60 );

		}

			// Authors qty
			$st_['authors_qty'] = count( $st_['authors'] );

		// Offset
		$st_['offset'] = ( $paged - 1 ) * $st_['per_page'];

		// Pages qty
		$st_['pages_qty'] = ceil( $st_['authors_qty'] / $st_['per_page'] );



/*===============================================

	P A G E
	Display a required page

===============================================*/

	get_header();

		?>

			<div id="content-holder" class="sidebar-position-<?php echo $st_['sidebar_position']; ?>">

				<div id="content-box">
		
					<div>

						<div>

							<?php


								/*-------------------------------------------
									2.1 - Default content
								-------------------------------------------*/

								if ( have_posts() ) {
			
									while ( have_posts() ) : the_post();

										if ( !$st_['title_disabled'] || get_the_content() ) {

											// Title
											if ( !$st_['title_disabled'] && !is_front_page() ) {
												echo '<h1 class="entry-title page-title">' . get_the_title() . ( $st_['subtitle'] ? ' <span class="title-sub">' . $st_['subtitle'] . '</span>' : '' ) . '</h1>'; }

											// Content
											if ( get_the_content() ) {
												echo '<article><div id="article">'; the_content(); echo '</div></article>'; }
		
											echo '<div class="clear"><!-- --></div>';

										}

									endwhile;
			
								}


								/*-------------------------------------------
									2.2 - Authors
								-------------------------------------------*/

								if ( $st_['offset'] > 0 ) {
									$st_['authors'] = array_slice( $st_['authors'], $st_['offset'] ); }

								foreach ( $st_['authors'] as $st_['key'] ) {

									$st_['count']++;

									include( locate_template( '/includes/posts/formats/status.php' ) );

									if ( $st_['count'] == $st_['per_page'] ) {
										break; }

								}


								/*-------------------------------------------
									2.3 - Pagination
								-------------------------------------------*/

								if ( $st_['authors_qty'] > $st_['per_page'] ) {
								
									$st_['current'] = max( 1, $paged );
								
									?>
								
										<div class="clear"><!-- --></div>
								
										<div id="wp-pagenavibox">
											<div class="wp-pagenavi">
								
												<span class="pages"><?php printf( __( 'Page %d of %d', 'strictthemes' ), $st_['current'], $st_['pages_qty'] ); ?></span>
								
												<?php
													echo paginate_links(
														array (
															'base'		=>	get_pagenum_link(1) . '%_%',
															'format'	=>	'page/%#%/',
															'current'	=>	$st_['current'],
															'total'		=>	$st_['pages_qty'],
															'prev_next'	=>	true,
														)
													);
												?>
								
											</div>
										</div>
								
									<?php
								
								}


							?>

							<div class="clear"><!-- --></div>

						</div>

					</div>
		
				</div><!-- #content-box -->

				<div class="clear"><!-- --></div>

			</div><!-- #content-holder -->
	
		<?php

	get_footer();

?>