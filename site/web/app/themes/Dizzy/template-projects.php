<?php if ( !defined( 'ABSPATH' ) ) exit;

/*

	Template Name: Projects

	1 - RETRIEVE DATA

	2 - PROJECTS

		2.1 - Title
		2.2 - Projects
			- Loop

*/

/*===============================================

	R E T R I E V E   D A T A
	Get a required page data

===============================================*/

	global
		$st_Settings,
		$st_Options;

		$st_ = array();

		// Template name
		$st_['t'] = $st_Options['panel']['projects']['portfolio']['template-default'];
	
		// Sidebar position
		$st_['sidebar_position'] = 'none';

		// Post type names
		$st_['st_post'] = !empty( $st_Settings['ctp_post'] ) ? $st_Settings['ctp_post'] : $st_Options['ctp']['post'];
		$st_['st_category'] = !empty( $st_Settings['ctp_category'] ) ? $st_Settings['ctp_category'] : $st_Options['ctp']['category'];
		$st_['st_tag'] = !empty( $st_Settings['ctp_tag'] ) ? $st_Settings['ctp_tag'] : $st_Options['ctp']['tag'];

		// Projects check
		$st_['is_projects'] = ( function_exists( 'st_kit' ) && !empty( $st_Settings['projects_status'] ) == 'yes' ) ? true : false;

		// Projects per page
		$st_['projects_per_page'] = !empty( $st_Settings['projects_qty'] ) ? $st_Settings['projects_qty'] : $st_Options['ctp']['qty'];

		// Projects categories
		$st_['st_categories'] = get_terms( $st_['st_category'], array( 'hierarchical' => false ) );

		// Paged
		if ( get_query_var('paged') ) {
			$paged = get_query_var('paged'); }
		
		elseif ( get_query_var('page') ) {
			$paged = get_query_var('page'); }
		
		else {
			$paged = 1; }
		
		$st_['paged'] = $paged;
	
		// Is title disabled?
		$st_['title_disabled'] = st_get_post_meta( $post->ID, 'disable_title_value', true, 0 );

		// Subtitle
		$st_['subtitle'] = get_post_meta( $post->ID, 'subtitle_value', true );


/*===============================================

	P R O J E C T S
	Display projects archive

===============================================*/

	get_header();

		?>
			
			<div id="content-holder" class="projects projects-<?php echo $st_['t']; ?> sidebar-position-<?php echo $st_['sidebar_position']; ?>">
		
				<div id="content-box">
		
					<div>

						<div>

							<?php

								/*-------------------------------------------
									2.1 - Title
								-------------------------------------------*/

								echo '<div id="projects-term">';

									// Title
									if ( !is_front_page() && $st_['title_disabled'] != true ) {
										echo '<h1 class="projects-title">&nbsp; ' . get_the_title() . ( $st_['subtitle'] ? ' <span class="title-sub">' . $st_['subtitle'] . '</span>' : '' ) . ' &nbsp;</h1>'; }

									// Projects selector
									if ( $st_['st_categories'] ) {
									
										echo '<ul id="projects-selector">';

											echo '<li><a href="#" class="selected" data-slug="all">' . __( 'All', 'strictthemes' ) . '</a></li>';

											foreach( $st_['st_categories'] as $st_['key'] ) {
												echo '<li><a href="' . get_term_link( $st_['key'] ) . '" data-slug="' . $st_['key']->slug . '">' . $st_['key']->name . '</a></li>'; }

										echo '</ul>';

									}

								echo '</div>';


								/*-------------------------------------------
									2.2 - Projects
								-------------------------------------------*/

								if ( $st_['is_projects'] ) {

									$st_['args'] = array(
										'post_type'			=> $st_['st_post'],
										'posts_per_page'	=> $st_['projects_per_page'],
										'order'				=> 'DESC',
										'paged'				=> $st_['paged'],
										'post_status'		=> 'publish'
									);


									$ctp_query = new WP_Query( $st_['args'] ); ?>
								
								
									<div class="projects-<?php echo $st_['t'] ?>-wrapper" id="projects-selection">
								
										<?php
								
											$st_['postcount'] = 0;
											$st_['featcount'] = 0;
											$st_['odd_even'] = 'odd';
								
											while ( $ctp_query->have_posts() ) : $ctp_query->the_post();
								
												$st_['postcount']++;
								
												include( locate_template( 'includes/projects/' . $st_['t'] . '.php' ) );
								
											endwhile;
								
										?>
								
										<div class="clear"><!-- --></div>
								
									</div><!-- .projects-$st_['t']-wrapper --><?php
								
								
									// Pagination
									if ( function_exists('wp_pagenavi') ) {
										?><div id="wp-pagenavibox"><?php wp_pagenavi( array( 'query' => $ctp_query ) ); ?></div><?php }
									else {
										?><div id="but-prev-next"><?php next_posts_link( __( 'Older posts', 'strictthemes' ), $ctp_query->max_num_pages ); previous_posts_link( __( 'Newer posts', 'strictthemes' ) ); ?></div><?php }


								}

								else {

									echo '<p>' . __( 'Portfolio inactive. Turn On <strong>Projects</strong> at <em>Theme Panel > Projects</em> page.', 'strictthemes' ) . '</p>';

								}

							?>

							<div class="clear"><!-- --></div>

						</div>

					</div>
		
				</div><!-- #content-box -->
				
			</div><!-- #content-holder -->

		<?php

	get_footer();

?>