<?php if ( !defined( 'ABSPATH' ) ) exit;

/*

	1 - FEATURED POSTS

*/



/*===============================================

	F E A T U R E D   P O S T S
	Sticky posts

===============================================*/

	global
		$st_Settings,
		$st_Options,
		$paged;

		$st_['is_featured'] = false;
		$st_['is_others'] = false;
		$st_['sticky_posts'] = array_filter( get_option('sticky_posts') );


		// If no one sticky post
		if ( empty( $st_['sticky_posts'] ) ) {
			return; }


		// If frontpage
		if ( is_front_page() && !empty( $st_Settings['sticky_on_frontpage'] ) == 'yes' && $paged == 1 ) {
			$st_['is_featured'] = true; }


		// If archives
		if (
			is_category() && !empty( $st_Settings['sticky_on_archives'] ) == 'yes' ||
			is_tag() && !empty( $st_Settings['sticky_on_archives'] ) == 'yes' ||
			get_query_var('post_format') && !empty( $st_Settings['sticky_on_archives'] ) == 'yes'
			) {

				$st_['is_featured'] = true;
				$st_['object'] = get_queried_object();

				// Unique args
				$st_['args'] = array(
					'post__in'				=> $st_['sticky_posts'],
					'posts_per_page'		=> 5,
					'order'					=> 'DESC',
					'orderby'				=> 'date',
					'paged'					=> 1,
					'post_status'			=> 'publish',
					'ignore_sticky_posts'	=> 1,
					'tax_query'				=> array(
													array(
														'taxonomy'	=> $st_['object']->taxonomy,
														'field'		=> 'term_id',
														'terms'		=> $st_['object']->term_id
													)
												)
				);

			}


		// Continue or not?
		if ( $st_['is_featured'] == false ) {
			return; }


		// Default args
		if ( empty( $st_['object'] ) ) {

				$st_['args'] = array(
					'post__in'				=> $st_['sticky_posts'],
					'posts_per_page'		=> 5,
					'order'					=> 'DESC',
					'orderby'				=> 'date',
					'paged'					=> 1,
					'post_status'			=> 'publish',
					'ignore_sticky_posts'	=> 1
				);

		}


		$st_['postcount'] = 1;
		$st_['odd_even'] = 'odd';
		$st_['posts-sticky-a'] = '';
		$st_['posts-sticky-b'] = '';
		$st_['feat_type'] = !empty( $st_['object'] ) ? $st_['object']->taxonomy . '_' . $st_['object']->term_id : 'recent';


		$st_['temp'] = !empty( $st_query ) ? $st_query : '';
		$st_query = null;


		if ( !empty( $st_Settings['sticky_cache'] ) ) {
			$st_query = get_transient( 'st_sticky_posts_' . $st_['feat_type'] ); }


		if ( $st_query == false ) {

			$st_query = new WP_Query( $st_['args'] );

			set_transient( 'st_sticky_posts_' . $st_['feat_type'], $st_query, 60 * 60 * 12 );

		}


		if ( $st_query->found_posts ) {

			?>

				<div id="posts-sticky">
				
					<div class="posts-sticky">
		
						<?php
		
							while ( $st_query->have_posts() ) : $st_query->the_post();

								// Post format
								$st_['format'] = ( get_post_format( $post->ID ) && $st_Options['global']['post-formats'][get_post_format( $post->ID )]['status'] && function_exists( 'st_kit' ) ) ? get_post_format( $post->ID ) : 'standard';

								// Feat image
								if ( has_post_thumbnail() ) {
							
									$st_['id'] = get_post_thumbnail_id( $post->ID );
									$st_['thumb'] = wp_get_attachment_image_src( $st_['id'], $st_['postcount'] == 1 ? 'project-medium' : 'project-thumb' );
									$st_['thumb'] = $st_['thumb'][0];
									$st_['is_thumb'] = true;

								}
							
								else {
							
									$st_['thumb'] = get_template_directory_uri() . '/assets/images/placeholder.png';
									$st_['is_thumb'] = false;
							
								}

								// posts-sticky-a
								if ( $st_['postcount'] < 4 ) {

									if ( $st_['postcount'] == 1 ) {

										// Image for bg
										if ( $st_['is_thumb'] == true ) {

											$st_['large'] = wp_get_attachment_image_src( $st_['id'], 'large' );
											$st_['large'] = $st_['large'][0];

										}

										// Highlighted
										$st_['posts-sticky-a'] .=
											'<div class="post-sticky-recent">' .
												'<a ' . ( function_exists( 'st_get_2x' ) ? st_get_2x( $post->ID, 'project-medium', 'attr' ) : '' ) . ' style="background-image: url(' . $st_['thumb'] . ')"' . ( $st_['is_thumb'] == true ? ' data-bg="' . $st_['large'] . '"' : '' ) . ' href="' . get_permalink() . '">' .
													'<div>' .
														( function_exists( 'wp_review_show_total' ) ? wp_review_show_total( false ) : '' ) . "\n" .
														'<h1 class="format-after format-' . $st_['format'] . '-after">' . get_the_title() . '</h1>' . "\n" .
														wpautop( do_shortcode( get_the_excerpt() ) ) .
													'</div>' .
												'</a>' .
											'</div>' . "\n";

									}

									else {

										// Regular
										$st_['posts-sticky-a'] .=
											'<div class="post-sticky ' . $st_['odd_even'] . '">' .
												'<a ' . ( function_exists( 'st_get_2x' ) ? st_get_2x( $post->ID, 'project-thumb', 'attr' ) : '' ) . ' style="background-image: url(' . $st_['thumb'] . ')" href="' . get_permalink() . '">' .
													'<div>' .
														( function_exists( 'wp_review_show_total' ) ? wp_review_show_total( false ) : '' ) .
														'<h3 class="format-after format-' . $st_['format'] . '-after">' . get_the_title() . '</h3>' .
													'</div>' .
												'</a>' .
											'</div>' . "\n";

										$st_['odd_even'] = $st_['odd_even'] == 'odd' ? 'even' : 'odd';

									}

								}

								// posts-sticky-b
								else {

									// Regular
									$st_['posts-sticky-b'] .=
										'<div class="post-sticky ' . $st_['odd_even'] . '">' .
											'<a ' . ( function_exists( 'st_get_2x' ) ? st_get_2x( $post->ID, 'project-thumb', 'attr' ) : '' ) . ' style="background-image: url(' . $st_['thumb'] . ')" href="' . get_permalink() . '">' .
												'<div>' .
													( function_exists( 'wp_review_show_total' ) ? wp_review_show_total( false ) : '' ) .
													'<h3 class="format-after format-' . $st_['format'] . '-after">' . get_the_title() . '</h3>' .
												'</div>' .
											'</a>' .
										'</div>' . "\n";

									$st_['odd_even'] = $st_['odd_even'] == 'odd' ? 'even' : 'odd';

								}

								$st_['postcount']++;
	
							endwhile;

							if ( $st_['posts-sticky-a'] ) {
								echo '<div class="posts-sticky-a">' . "\n" . $st_['posts-sticky-a'] . '</div><!-- .posts-sticky-a -->' . "\n"; }

							if ( $st_['posts-sticky-b'] ) {
								echo '<div class="posts-sticky-b">' . "\n" . $st_['posts-sticky-b'] . '</div><!-- .posts-sticky-b -->' . "\n"; }

						?>
		
						<div class="clear"><!-- --></div>
					</div><!-- .posts-sticky -->
				
				</div><!-- #posts-sticky -->

			<?php

		}

		$st_query = null;
		$st_query = $st_['temp'];
		wp_reset_query();


?>