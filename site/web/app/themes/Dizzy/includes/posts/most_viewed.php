<?php if ( !defined( 'ABSPATH' ) ) exit;

/*

	1 - MOST VIEWED POSTS

*/

/*===============================================

	M O S T   V I E W E D   P O S T S
	Most viewed posts

===============================================*/

	global
		$st_Settings,
		$st_Options,
		$paged;

		$st_['is_most_viewed'] = false;
		$st_['is_others'] = false;


		// If frontpage
		if ( is_front_page() && !empty( $st_Settings['most_viewed_on_frontpage'] ) == 'yes' && $paged == 1 ) {
			$st_['is_most_viewed'] = true; }


		// If archives
		if (
			is_category() && !empty( $st_Settings['most_viewed_on_archives'] ) == 'yes' ||
			is_tag() && !empty( $st_Settings['most_viewed_on_archives'] ) == 'yes' ||
			get_query_var('post_format') && !empty( $st_Settings['most_viewed_on_archives'] ) == 'yes'
			) {

				$st_['is_most_viewed'] = true;
				$st_['timeframe'] = !empty( $st_Settings['most_viewed_period'] ) ? $st_Settings['most_viewed_period'] : 'all';
				$st_['object'] = get_queried_object();

				// Unique args
				$st_['args'] = array(
					'posts_per_page'		=> 4,
					'ignore_sticky_posts'	=> 1,
					'orderby'				=> 'meta_value_num',
					'meta_key'				=> 'post_views_count',
					'date_query'			=> ( $st_['timeframe'] != 'all' ? 
														array(
															array(
																'column'	=> 'post_date_gmt',
																'after'		=> '1 ' . $st_['timeframe'] . ' ago',
															),
														)
												: '' ),
					'tax_query'				=> array(
													array(
														'taxonomy'	=> $st_['object']->taxonomy,
														'field'		=> 'term_id',
														'terms'		=> $st_['object']->term_id
													)
												)
				);

			}


		// If single
		if (
			is_single() && !empty( $st_Settings['most_viewed_on_single'] ) == 'yes' && get_post_type() == 'post' ||
			is_single() && !empty( $st_Settings['most_viewed_on_single'] ) == 'yes' && get_post_type() == 'post' && function_exists('is_bbpress') && !is_bbpress()
			) {
				$st_['is_most_viewed'] = true;
			}


		// If others
		if ( !is_front_page() && !is_category() && !is_tag() && !is_single() && !get_query_var('post_format') ) {
			$st_['is_others'] = true; }

		if ( $st_['is_others'] && !empty( $st_Settings['most_viewed_on_others'] ) == 'yes' ) {
			$st_['is_most_viewed'] = true; }


		// Continue or not?
		if ( $st_['is_most_viewed'] == false ) {
			return; }


		$st_['timeframe'] = !empty( $st_Settings['most_viewed_period'] ) ? $st_Settings['most_viewed_period'] : 'all';


		// Default args
		if ( empty( $st_['object'] ) ) {

			$st_['args'] = array (
				'posts_per_page'		=> 4,
				'ignore_sticky_posts'	=> 1,
				'orderby'				=> 'meta_value_num',
				'meta_key'				=> 'post_views_count',
				'date_query'			=> ( $st_['timeframe'] != 'all' ? 
													array(
														array(
															'column'	=> 'post_date_gmt',
															'after'		=> '1 ' . $st_['timeframe'] . ' ago',
														),
													)
											: '' )
			);

		}

		$st_['postcount'] = 0;
		$st_['odd_even'] = 'odd';
		$st_['feat_type'] = !empty( $st_['object'] ) ? $st_['object']->taxonomy . '_' . $st_['object']->term_id : 'recent';


		$st_['temp'] = !empty( $st_query ) ? $st_query : '';
		$st_query = null;


		if ( !empty( $st_Settings['most_viewed_cache'] ) ) {
			$st_query = get_transient( 'st_most_viewed_posts_' . $st_['feat_type'] ); }


		if ( $st_query == false ) {

			$st_query = new WP_Query( $st_['args'] );

			set_transient( 'st_most_viewed_posts_' . $st_['feat_type'], $st_query, 60 * 60 * 12 );

		}


		echo '<div id="posts-most-viewed">';
	
			while ( $st_query->have_posts() ) : $st_query->the_post();		

				$st_['postcount']++;

				// Post format
				$st_['format'] = ( get_post_format( $post->ID ) && $st_Options['global']['post-formats'][get_post_format( $post->ID )]['status'] && function_exists( 'st_kit' ) ) ? get_post_format( $post->ID ) : 'standard';
			
				// Post's class
				$st_['class'] = '';
				if ( $st_['postcount'] == 1 ) { $st_['class'] = ' first'; }
				if ( $st_['postcount'] == 4 ) { $st_['class'] = ' last'; $st_['postcount'] = 0; }

				// Odd or even
				$st_['class'] .= ' ' . $st_['odd_even'];
				$st_['odd_even'] = $st_['odd_even'] == 'odd' ? 'even' : 'odd';

				// Feat image
				if ( has_post_thumbnail() ) {
			
					$st_['id'] = get_post_thumbnail_id( $post->ID );
					$st_['thumb'] = wp_get_attachment_image_src( $st_['id'], 'project-thumb' );
					$st_['thumb'] = $st_['thumb'][0];
			
				}
			
				else {
			
					$st_['thumb'] = get_template_directory_uri() . '/assets/images/placeholder.png';
			
				}
			
				echo

					// Compose post
					'<div class="post-most-viewed' . $st_['class'] . '">' .
				
						// Compose thumb
						'<a href="' . get_permalink() . '">' .
	
							'<div class="post-most-viewed-front">';
								st_post_meta( false, false, false, false, false, true, false );
								echo '<div class="post-most-viewed-details">' .
									( function_exists( 'wp_review_show_total' ) ? wp_review_show_total( false ) : '' ) . "\n" .
									'<h3 class="format-after format-' . $st_['format'] . '-after">' . get_the_title() . '</h3>' . "\n" .
								'</div>' .
							'</div>' .

							'<div class="post-most-viewed-back" ' . ( function_exists( 'st_get_2x' ) ? st_get_2x( $post->ID, 'project-thumb', 'attr' ) : '' ) . ' style="background-image: url(' . $st_['thumb'] . ')"><!-- Thumbnail --></div>' .
	
						'</a>' .
				
					'</div>' . "\n";
	
			endwhile;

		echo '<div class="clear"><!-- --></div></div>';


		$st_query = null;
		$st_query = $st_['temp'];
		wp_reset_query();


?>