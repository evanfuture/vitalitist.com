<?php if ( !defined( 'ABSPATH' ) ) exit;
/*

	Post formats compatible.

*/

	// Define classes
	$st_['class'] = has_post_thumbnail() ? 'post-template post-t6 post-t6-yes-thumb' : 'post-template post-t6 post-t6-no-thumb';

	// Odd or Even?
	$st_['class'] .= $st_['count'] % 2 == 0 ? ' even' : ' odd';

	// Last?
	$st_['class'] .= $st_['count'] % 3 == 0 ? ' third' : '';

	// Forth?
	$st_['class'] .= $st_['count'] % 4 == 0 ? ' forth' : '';

?>
<div class="<?php echo $st_['class']; ?>">

	<?php

		global
			$st_Options,
			$st_Settings;

			// Post format
			$st_['format'] = ( get_post_format( $post->ID ) && $st_Options['global']['post-formats'][get_post_format( $post->ID )]['status'] && function_exists( 'st_kit' ) ) ? get_post_format( $post->ID ) : 'standard';

			// Read More button
			$st_['readmore'] = array(
				'standard'	=> __( 'Read', 'strictthemes' ),
				'image'		=> __( 'View image', 'strictthemes' ),
				'gallery'	=> __( 'Browse gallery', 'strictthemes' ),
				'audio'		=> __( 'Listen audio', 'strictthemes' ),
				'video'		=> __( 'Watch video', 'strictthemes' ),
				'link'		=> __( 'Get link', 'strictthemes' ),
				'quote'		=> __( 'More about', 'strictthemes' ),
				'status'	=> __( 'More about', 'strictthemes' )
			);

			// Is title disabled?
			$st_['title_disabled'] = st_get_post_meta( $post->ID, 'disable_title_value', true, 0 );

	
			if ( has_post_thumbnail() ) {
	
				$st_['id'] = get_post_thumbnail_id( $post->ID );
				$st_['thumb'] = wp_get_attachment_image_src( $st_['id'], 'project-thumb' );
				$st_['thumb'] = $st_['thumb'][0];

				echo '<div class="thumb-wrapper">';

					// Meta
					if ( !empty( $st_Settings['post_meta'] ) && $st_Settings['post_meta'] == 'yes' ) {
						st_post_meta( false, true, true, false, true, false, false ); }

					// Thumbnail
					echo '<a href="' . get_permalink() . '" class="post-thumb post-thumb-' . $st_['format'] . '" ' . ( function_exists( 'st_get_2x' ) ? st_get_2x( $post->ID, 'project-thumb', 'attr' ) : '' ) . ' style="background-image: url(' . $st_['thumb'] . ')" data-format="' . $st_['format'] . '">';

						// Review
						if ( function_exists( 'wp_review_show_total' ) ) {
							wp_review_show_total(); }

					echo '</a>';

				echo '</div>' . "\n";
		
			}
	
			echo '<div class="post-t6-details">';

				if ( !has_post_thumbnail() ) {

					// Review
					if ( function_exists( 'wp_review_show_total' ) ) {
						wp_review_show_total(); }

				}

				// Title and Excerpt
				if ( !empty( $st_['title_disabled'] ) != 1 ) {
					echo '<h3 class="post-title format-after format-' . $st_['format'] . '-after"><a href="' . get_permalink() . '">' . get_the_title() . '</a></h3>'; }

				echo '<div>' . wpautop( do_shortcode( get_the_excerpt() ) ) . '</div>';

			echo '</div>';

			echo '<a class="post-t6-more" href="' . get_permalink() . '">' . $st_['readmore'][$st_['format']] . '</a>';

			if ( !has_post_thumbnail() ) {

				// Meta for non-thumb
				if ( !empty( $st_Settings['post_meta'] ) && $st_Settings['post_meta'] == 'yes' ) {
					st_post_meta( false, true, true, false, true, false, false ); }

			}

	?>

	<div class="clear"><!-- --></div>

</div><!-- .post-template -->