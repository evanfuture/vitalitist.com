<?php if ( !defined( 'ABSPATH' ) ) exit;

/*

	Template Name: Projects

	1 - RETRIEVE DATA

	2 - PROJECTS

		2.1 - Query
		2.2 - Loop

*/

/*===============================================

	R E T R I E V E   D A T A
	Get a required page data

===============================================*/

	global
		$st_Settings,
		$st_Options;

		$st_ = array();

		$st_['is_projects_another'] = false;
		$st_['is_others'] = false;

		// Paged
		$st_['paged'] = 1;


		// Post type names
		$st_['st_post'] = !empty( $st_Settings['ctp_post'] ) ? $st_Settings['ctp_post'] : $st_Options['ctp']['post'];
		$st_['st_category'] = !empty( $st_Settings['ctp_category'] ) ? $st_Settings['ctp_category'] : $st_Options['ctp']['category'];
		$st_['st_tag'] = !empty( $st_Settings['ctp_tag'] ) ? $st_Settings['ctp_tag'] : $st_Options['ctp']['tag'];


		// If frontpage
		if ( is_front_page() && !empty( $st_Settings['projects_another_on-frontpage'] ) == 'yes' && $st_['paged'] == 1 ) {
			$st_['is_projects_another'] = true; }


		// If archives
		if (
			is_category() && !empty( $st_Settings['projects_another_on-archives'] ) == 'yes' ||
			is_tag() && !empty( $st_Settings['projects_another_on-archives'] ) == 'yes' ||
			get_query_var('post_format') && !empty( $st_Settings['projects_another_on-archives'] ) == 'yes'
			) { $st_['is_projects_another'] = true; }


		// If single
		if (
			is_single() && !empty( $st_Settings['projects_another_on-single'] ) == 'yes' && get_post_type() == $st_['st_post'] ||
			is_single() && !empty( $st_Settings['projects_another_on-single'] ) == 'yes' && get_post_type() == $st_['st_post'] && function_exists('is_bbpress') && !is_bbpress()
			) { $st_['is_projects_another'] = true; }

		// If others
		if ( !is_front_page() && !is_category() && !is_tag() && !is_single() && !get_query_var( 'post_format' ) && get_queried_object_id() != st_get_page_by_template( 'template-projects' ) && get_post_type() != $st_['st_post'] ) {
			$st_['is_others'] = true; }

		if ( $st_['is_others'] && !empty( $st_Settings['projects_another_on-others'] ) == 'yes' ) {
			$st_['is_projects_another'] = true; }


		// Continue or not?
		if ( $st_['is_projects_another'] == false ) {
			return; }


		// Template name
		$st_['t'] = $st_Options['panel']['projects']['portfolio']['template-default'];


		// Projects check
		$st_['is_projects'] = ( function_exists( 'st_kit' ) && !empty( $st_Settings['projects_status'] ) == 'yes' ) ? true : false;


		// Projects per page
		$st_['projects_per_page'] = !empty( $st_Settings['projects_another_qty'] ) ? $st_Settings['projects_another_qty'] : $st_Options['ctp']['qty_another'];


		// Projects order
		$st_['st_orderby'] = !empty( $st_Settings['projects_another_type'] ) ? $st_Settings['projects_another_type'] : 'date';



/*===============================================

	P R O J E C T S
	Display projects archive

===============================================*/

	/*-------------------------------------------
		2.1 - Query
	-------------------------------------------*/

	$st_['prefix'] = '';

	$st_['args'] = array(
		'post_type'			=> $st_['st_post'],
		'posts_per_page'	=> $st_['projects_per_page'],
		'order'				=> 'DESC',
		'orderby'			=> $st_['st_orderby'],
		'paged'				=> $st_['paged'],
		'post_status'		=> 'publish',
	);

	// Exclude current project from another projects
	if ( is_single() && !empty( $st_Settings['projects_another_on-single'] ) == 'yes' && get_post_type() == $st_['st_post'] ) {

		$st_['args']['post__not_in'] = array( $post->ID );

		$st_['prefix'] = $post->ID . '_';

	}


	/*-------------------------------------------
		2.2 - Loop
	-------------------------------------------*/

	if ( $st_['is_projects'] ) {

		$ctp_query = null;

		if ( !empty( $st_Settings['projects_another_cache'] ) ) {
			$ctp_query = get_transient( 'st_projects_another_' . $st_['prefix'] . $st_['st_orderby'] ); }

		if ( $ctp_query == false ) {

			$ctp_query = new WP_Query( $st_['args'] );

			set_transient( 'st_projects_another_' . $st_['prefix'] . $st_['st_orderby'], $ctp_query, 60 * 60 * 12 );

		} ?>
	
	
		<div class="projects-<?php echo $st_['t'] ?>-wrapper" id="projects-another">

			<div class="separator-or"><span>&nbsp; <?php _e( 'Portfolio', 'strictthemes' ) ?> &nbsp;</span></div>

			<?php

				$st_['postcount'] = 0;
				$st_['featcount'] = 0;
				$st_['odd_even'] = 'odd';
	
				while ( $ctp_query->have_posts() ) : $ctp_query->the_post();
	
					$st_['postcount']++;
	
					include( locate_template( 'includes/projects/' . $st_['t'] . '.php' ) );

				endwhile;

				if ( $st_['projects_per_page'] < $ctp_query->found_posts && st_get_page_by_template( 'template-projects' ) ) {
					echo '<div id="button-projects"><a href="' . get_permalink( st_get_page_by_template( 'template-projects' ) ) . '" class="button-st button-with-icon button-with-icon-16 ico-st ico-portfolio">' . __( 'Browse all', 'strictthemes' ) . '</a></div>';
				}
	
			?>

			<div class="clear"><!-- --></div>
	
		</div><!-- .projects-$st_['t']-wrapper --><?php
	
	}

	else {

		echo '<p>' . __( 'Portfolio inactive. Turn On <strong>Projects</strong> at <em>Theme Panel > Projects</em> page.', 'strictthemes' ) . '</p>';

	}

?>