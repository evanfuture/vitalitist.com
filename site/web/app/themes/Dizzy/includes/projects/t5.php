<?php if ( !defined( 'ABSPATH' ) ) exit;

	// Post format
	$st_['st_ctp_format'] = !empty( $st_Options['ctp']['ctp-formats']['enabled'] ) ? $st_Options['ctp']['ctp-formats']['formats']['tag'] : false;
	$st_['format'] = strtolower( st_wp_get_post_terms( $post->ID, $st_['st_ctp_format'], false ) );

	// Post's class
	$st_['class'] = '';
	if ( $st_['postcount'] == 1 ) { $st_['class'] = ' first'; }
	if ( $st_['postcount'] == 4 ) { $st_['class'] = ' last'; $st_['postcount'] = 0; }

	// Odd or even
	$st_['class'] .= ' ' . $st_['odd_even'];
	$st_['odd_even'] = $st_['odd_even'] == 'odd' ? 'even' : 'odd';

	// Feat image
	if ( has_post_thumbnail() ) {

		$st_['id'] = get_post_thumbnail_id( $post->ID );
		$st_['thumb'] = wp_get_attachment_image_src( $st_['id'], 'project-thumb' );
		$st_['thumb'] = $st_['thumb'][0];

	}

	else {

		$st_['thumb'] = get_template_directory_uri() . '/assets/images/placeholder.png';

	}


	echo

		// Compose post
		'<div class="project-t5' . $st_['class'] . '" data-category="' . st_wp_get_post_terms( $post->ID, $st_['st_category'], false, 'slug', ' ' ) . '">' .
	
			// Compose thumb
			'<a href="' . get_permalink() . '">' .

				'<div class="project-t5-front">';
					st_post_meta( false, false, false, false, false, true, false );
					echo '<div class="project-t5-details">' .
						( function_exists( 'wp_review_show_total' ) ? wp_review_show_total( false ) : '' ) . "\n" .
						'<h3 class="format-after format-' . $st_['format'] . '-after">' . get_the_title() . '</h3>' . "\n" .
					'</div>' .
				'</div>' .

				'<div class="project-t5-back" ' . ( function_exists( 'st_get_2x' ) ? st_get_2x( $post->ID, 'project-thumb', 'attr' ) : '' ) . ' style="background-image: url(' . $st_['thumb'] . ')"><!-- Thumbnail --></div>' .

			'</a>' .
	
		'</div>' . "\n";

?>