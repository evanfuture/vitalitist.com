<?php if ( !defined( 'ABSPATH' ) ) exit;

/*

	1 - PPOJECTS

		1.1 - Breadcrumbs
		1.2 - Term title
		1.3 - Loop

*/

/*===============================================

	R E T R I E V E   D A T A
	Get a required page data

===============================================*/

	global
		$st_Options,
		$st_Settings,
		$paged;

		$st_ = array();
		$st_['args'] = array();

		$st_['count'] = 0;

		// Post type names
		$st_['st_post'] = !empty( $st_Settings['ctp_post'] ) ? $st_Settings['ctp_post'] : $st_Options['ctp']['post'];

		// Taxonomy
		$st_['taxonomy'] = get_queried_object()->taxonomy;
		$st_['taxonomy_value'] = get_queried_object()->slug;

		// Template name
		$st_['t'] = $st_Options['panel']['projects']['portfolio']['template-default'];
	
		// Sidebar position
		$st_['sidebar_position'] = 'none';

		$st_['st_category'] = !empty( $st_Settings['ctp_category'] ) ? $st_Settings['ctp_category'] : $st_Options['ctp']['category'];
		$st_['st_tag'] = !empty( $st_Settings['ctp_tag'] ) ? $st_Settings['ctp_tag'] : $st_Options['ctp']['tag'];

		// Projects check
		$st_['is_projects'] = ( function_exists( 'st_kit' ) && !empty( $st_Settings['projects_status'] ) == 'yes' ) ? true : false;

		// Projects per page
		$st_['projects_per_page'] = !empty( $st_Settings['projects_qty'] ) ? $st_Settings['projects_qty'] : $st_Options['ctp']['qty']; 

		// Paged
		$st_['paged'] = get_query_var('paged') ? get_query_var('paged') : 0;


/*===============================================

	P R O J E C T S
	Display projects archive

===============================================*/

	get_header();

		?>

			<div id="content-holder" class="projects projects-<?php echo $st_['t']; ?> sidebar-position-<?php echo $st_['sidebar_position']; ?>">
		
				<div id="content-box">
		
					<div>

						<div>

							<?php

								if ( $st_['is_projects'] ) { ?>

									<div id="projects-term">

										<h1 class="projects-title">&nbsp; <?php echo get_queried_object()->name; ?> &nbsp;</h1>

										<?php echo !empty( get_queried_object()->description ) ? '<div class="term-description">' . get_queried_object()->description . '</div>' : '' ?>

									</div>

									<div class="projects-<?php echo $st_['t'] ?>-wrapper">
								
										<?php
								
											$st_['postcount'] = 0;
											$st_['featcount'] = 0;
											$st_['odd_even'] = 'odd';
								
											while ( have_posts() ) : the_post();
								
												$st_['postcount']++;
								
												include( locate_template( 'includes/projects/' . $st_['t'] . '.php' ) );
								
											endwhile;

										?>
								
										<div class="clear"><!-- --></div>
								
									</div><!-- .projects-$st_['t']-wrapper --><?php
								

									// Pagination
									if ( function_exists('wp_pagenavi') ) {
										?><div id="wp-pagenavibox"><?php wp_pagenavi(); ?></div><?php }
									else {
										?><div id="but-prev-next"><?php next_posts_link( __( 'Older posts', 'strictthemes' ) ); previous_posts_link( __( 'Newer posts', 'strictthemes' ) ); ?></div><?php }

								}

								else {

									echo '<p>' . __( 'Portfolio inactive. Turn On <strong>Projects</strong> at <em>Theme Panel > Projects</em> page.', 'strictthemes' ) . '</p>';

								}

							?>

							<div class="clear"><!-- --></div>

						</div>

					</div>
		
				</div><!-- #content-box -->
				
			</div><!-- #content-holder -->

		<?php
	
	get_footer();

?>