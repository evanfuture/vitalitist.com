<?php if ( !defined( 'ABSPATH' ) ) exit;

/*

	Template Name: Frontpage v.2

	1 - RETRIEVE DATA

	2 - PAGE

		2.1 - Breadcrumbs
		2.2 - Article
			- Title
			- Content
		2.3 - Pagination
		2.4 - Comments
		2.5 - Sidebar

*/

/*===============================================

	R E T R I E V E   D A T A
	Get a required page data

===============================================*/

	global
		$st_Options,
		$st_Settings;

		$st_ = array();

		// Get sidebar position
		$st_['sidebar_position'] = 'none';

			// Re-define global $content_width if sidebar not exists
			if ( $st_['sidebar_position'] == 'none' ) {
				$content_width = $st_Options['global']['images']['large']['width']; }
			else {
				$content_width = $st_Options['global']['images']['archive-image']['width']; }

		// Paged
		if ( get_query_var('paged') ) {
			$paged = get_query_var('paged'); }
		
		elseif ( get_query_var('page') ) {
			$paged = get_query_var('page'); }
		
		else {
			$paged = 1; }
		
		$st_['paged'] = $paged;

		// Counter
		$st_['count'] = 0;


/*===============================================

	P A G E
	Display a required page

===============================================*/

	get_header();

		?>

			<div id="content-holder" class="sidebar-position-none">

				<div id="content-box">
		
					<div>

						<div>

							<?php

								if ( $paged == 1 ) {


									if ( have_posts() ) :
			
										while ( have_posts() ) : the_post(); 
	
	
	
											/*-------------------------------------------
												2.1 - Breadcrumbs
											-------------------------------------------*/
	
											/* n\a */
	
	
	
											/*-------------------------------------------
												2.2 - Article
											-------------------------------------------*/

											if ( get_the_content() ) { ?>
	
												<article>
		
													<div id="article-frontpage-2">
		
														<?php the_content(); ?>
		
														<div class="clear"><!-- --></div>
		
													</div><!-- #article -->
		
												</article>
		
												<div class="clear"><!-- --></div><?php

											}
	
								
	
											/*-------------------------------------------
												2.3 - Pagination
											-------------------------------------------*/
	
											/* n\a */
								
	
	
											/*-------------------------------------------
												2.4 - Comments
											-------------------------------------------*/
	
											/* n\a */
	
								
													
										endwhile;
	
									endif;
								
	
	
									/*-------------------------------------------
										2.5 - Sidebars Homepage
									-------------------------------------------*/
	
									if ( is_active_sidebar(7) || is_active_sidebar(8) || is_active_sidebar(9) || is_active_sidebar(10) ) { ?>
	
										<div id="sidebar-homepage-box">
		
											<div class="sidebar sidebar-homepage odd"><div>
												<?php if ( function_exists('dynamic_sidebar' ) && dynamic_sidebar( 'Homepage Sidebar 1' ) ); ?>
											</div></div>
								
											<div class="sidebar sidebar-homepage even"><div>
												<?php if ( function_exists('dynamic_sidebar' ) && dynamic_sidebar( 'Homepage Sidebar 2' ) ); ?>
											</div></div>
								
											<div class="sidebar sidebar-homepage odd"><div>
												<?php if ( function_exists('dynamic_sidebar' ) && dynamic_sidebar( 'Homepage Sidebar 3' ) ); ?>
											</div></div>
								
											<div class="sidebar sidebar-homepage even last"><div>
												<?php if ( function_exists('dynamic_sidebar' ) && dynamic_sidebar( 'Homepage Sidebar 4' ) ); ?>
											</div></div>
											
											<div class="clear"><!-- --></div>
		
										</div><?php
	
									}



								} // if ( $paged != 1 )



								/*-------------------------------------------
									2.1 - Recent Posts
								-------------------------------------------*/

								if ( $paged == 1 ) {
									echo '<div class="separator-or"><span>&nbsp; ' . __( 'Recent posts', 'strictthemes' ) . ' &nbsp;</span></div>'; } ?>

								<div id="posts-t6" class="v2">

									<?php


										$st_['qty'] = get_option( 'posts_per_page' );
		
										$st_['args_recent'] = array(
											'showposts'				=> $st_['qty'],
											'orderby'				=> 'date',
											'paged'					=> $paged,
											'post_status'			=> 'publish',
											'ignore_sticky_posts'	=> 1,
											'post__not_in'			=> !empty( $st_Settings['sticky_exclude'] ) && $st_Settings['sticky_exclude'] == 'yes' ? get_option('sticky_posts') : ''
										);

										$st_query = new WP_Query();
										$st_query->query( $st_['args_recent'] );

		
										/*--- Loop -----------------------------*/
		
										while ( $st_query->have_posts() ) : $st_query->the_post();

											$st_['count']++;

											include( locate_template( '/includes/posts/t6.php' ) );

										endwhile;
								

										// Pagination
										if ( function_exists('wp_pagenavi') ) {
											?><div id="wp-pagenavibox"><?php wp_pagenavi( array( 'query' => $st_query ) ); ?></div><?php }
										else {
											?><div id="but-prev-next"><?php next_posts_link( __( 'Older posts', 'strictthemes' ), $st_query->max_num_pages ); previous_posts_link( __( 'Newer posts', 'strictthemes' ) ); ?></div><?php }
		

									?>

								</div>

							<div class="clear"><!-- --></div>

						</div>

					</div>
			
				</div><!-- #content-box -->
	
				<?php

					/*-------------------------------------------
						2.5 - Sidebar
					-------------------------------------------*/

					/* n\a */

				?>

				<div class="clear"><!-- --></div>

			</div><!-- #content-holder -->
	
		<?php

	get_footer();

?>