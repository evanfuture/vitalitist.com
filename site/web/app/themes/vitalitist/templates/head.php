<?php use Vitalitist\Assets; ?>
<head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <?php wp_head(); ?>
  <link rel="icon" type="image/x-icon" href="<?= Assets\asset_path('images/favicons/favicon.ico') ?>" />
  <link rel="apple-touch-icon" sizes="57x57" href="<?= Assets\asset_path('images/favicons/apple-touch-icon-57x57.png')?>">
  <link rel="apple-touch-icon" sizes="60x60" href="<?= Assets\asset_path('images/favicons/apple-touch-icon-60x60.png')?>">
  <link rel="apple-touch-icon" sizes="72x72" href="<?= Assets\asset_path('images/favicons/apple-touch-icon-72x72.png')?>">
  <link rel="apple-touch-icon" sizes="76x76" href="<?= Assets\asset_path('images/favicons/apple-touch-icon-76x76.png')?>">
  <link rel="apple-touch-icon" sizes="114x114" href="<?= Assets\asset_path('images/favicons/apple-touch-icon-114x114.png')?>">
  <link rel="apple-touch-icon" sizes="120x120" href="<?= Assets\asset_path('images/favicons/apple-touch-icon-120x120.png')?>">
  <link rel="apple-touch-icon" sizes="144x144" href="<?= Assets\asset_path('images/favicons/apple-touch-icon-144x144.png')?>">
  <link rel="apple-touch-icon" sizes="152x152" href="<?= Assets\asset_path('images/favicons/apple-touch-icon-152x152.png')?>">
  <link rel="apple-touch-icon" sizes="180x180" href="<?= Assets\asset_path('images/favicons/apple-touch-icon-180x180.png')?>">
  <link rel="icon" type="image/png" href="<?= Assets\asset_path('images/favicons/favicon-32x32.png')?>" sizes="32x32">
  <link rel="icon" type="image/png" href="<?= Assets\asset_path('images/favicons/favicon-194x194.png')?>" sizes="194x194">
  <link rel="icon" type="image/png" href="<?= Assets\asset_path('images/favicons/favicon-96x96.png')?>" sizes="96x96">
  <link rel="icon" type="image/png" href="<?= Assets\asset_path('images/favicons/android-chrome-192x192.png')?>" sizes="192x192">
  <link rel="icon" type="image/png" href="<?= Assets\asset_path('images/favicons/favicon-16x16.png')?>" sizes="16x16">
  <link rel="manifest" href="<?= Assets\asset_path('images/favicons/manifest.json')?>">
  <meta name="apple-mobile-web-app-title" content="Vitalitist.com">
  <meta name="application-name" content="Vitalitist.com">
  <meta name="msapplication-TileColor" content="#603cba">
  <meta name="msapplication-TileImage" content="<?= Assets\asset_path('images/favicons/mstile-144x144.png')?>">
  <meta name="theme-color" content="#ffffff">

</head>
